# release-on-tag.gitlabpages.inria.fr

![Build Status](https://gitlab.inria.fr/gitlabci_gallery/release/release-on-tag.gitlabpages.inria.fr/badges/main/pipeline.svg)



This simple site illustrates an automated release deployment on gitlab pages.
Learn more about GitLab Pages at the [official documentation](https://docs.gitlab.com/ce/user/project/pages/).


The site uses a static part and a download part. The index.html of the download part are automatically generated based on the folder content.

The modification date displayed for each file is the date when the file was commited.

This project is part of https://gitlab.inria.fr/gitlabci_gallery/release/release-on-tag to illustrate a simple way to publish build artefact on an external git.


The resulting web site is visible at [https://gitlabci_gallery.gitlabpages.inria.fr/release/release-on-tag.gitlabpages.inria.fr](https://gitlabci_gallery.gitlabpages.inria.fr/release/release-on-tag.gitlabpages.inria.fr)

:bulb: Tip: if the project is hosted in a root group and if the project's name uses the following convention `<GroupName>.gitlabpages.inria.fr`, the final url will be shorten to `https://<GroupName>.gitlabpages.inria.fr` instead of `https://<GroupName>.gitlabpages.inria.fr/<ProjectName>`.

## Overview

The CI of this project and its companion project are organized as follow:

```mermaid
flowchart LR
    
    publish_in_remote_gitlabpages-- "commit" -->pages
    
    subgraph release-on-tag/.gitlab-ci.yml
        start1((.))-- "tag/commit" --> build-job
        build-job-- "tag" -->publish_in_generic_registry
        build-job-- "tag" -->publish_in_remote_gitlabpages
        publish_in_generic_registry-- "tag" -->create_release_job
    end
    subgraph release-on-tag.gitlabpages.inria.fr/.gitlab-ci.yml
        start2((.))-- "commit" --> pages
        pages
    end
```

## GitLab CI/CD

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/),
following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

It uses two scripts:

- [`scripts/fix_git_modification_date.sh`](fix_git_modification_date.sh) is in charge of changing the file modification time in the user workspace to the date of the last commit on the file.
- [`scripts/indexer.py`](indexer.py) is in charge of creating *index.html* for a part of the web site to display the folder content. (In this example, the *download* section).


### Indexer.py

`indexer.py` is actually a modification with small improvements of https://github.com/joshbrunty/Indexer script (under MIT License).

The presented version uses a basic bootstrap presentation. Feel free to adapt it, if you need to use it with other web site structures.

For basic use, you should update the default value defined on top of the file to adapt to your web site location ( or make sure to pass the value as arguments when calling the indexer)

For more advanced use, you'll need to adapt the html templates in the python script.

