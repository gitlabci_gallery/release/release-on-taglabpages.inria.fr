
#!/bin/sh

echo "Set modification time according to the file commit date of all files contained in `pwd`" 
# fix git modification dates (in order to correctly display them on the web using make_index.sh
# cf.https://stackoverflow.com/questions/2179722/checking-out-old-files-with-original-create-modified-timestamps
for FILE in $(git ls-files)
do
    TIME=$(git log --pretty=format:%cd -n 1 --date=iso $FILE)
    TIME2=`echo $TIME | sed 's/-//g;s/ //;s/://;s/:/\./;s/ .*//'`
    touch -m -t $TIME2 $FILE
done
