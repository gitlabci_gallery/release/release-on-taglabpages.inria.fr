#!/usr/bin/env python3

# Indexer v.1.0.2
# DESCRIPTION: This script generates an .html index  of files within a directory (recursive is OFF by default). Start from current dir or from folder passed as first positional argument. Optionally filter by file types with --filter "*.py". 

# version aadapted and modified from https://github.com/joshbrunty/Indexer
# Author: Josh Brunty (josh dot brunty at marshall dot edu)
# LICENSE: Mit

# -handle symlinked files and folders: displayed with custom icons
# By default only the current folder is processed.
# Use -r or --recursive to process nested folders.
# Use -i or --include-hidden to include hidden files and folders

import argparse
import datetime
import os
import sys
from pathlib import Path
from urllib.parse import quote

DEFAULT_OUTPUT_FILE = 'index.html'
DEFAULT_ROOT_PATH = '/release/release-on-tag.gitlabpages.inria.fr/'
DEFAULT_SOURCE_URL = 'https://gitlab.inria.fr/gitlabci_gallery/release/release-on-tag'
DEFAULT_TITLE = 'HelloGit - release-on-tag'


def process_dir(top_dir, opts):
    glob_patt = opts.filter or '*'

    path_top_dir: Path
    path_top_dir = Path(top_dir)
    index_file = None

    index_path = Path(path_top_dir, opts.output_file)

    if opts.verbose:
        print(f'Traversing dir {path_top_dir.absolute()}')

    try:
        index_file = open(index_path, 'w', encoding='utf-8')
    except Exception as e:
        print('cannot create file %s %s' % (index_path, e))
        return

    index_file.write("""<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>"""
        f'{opts.title} - {path_top_dir.name}'
    """
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	
	<!-- fontawesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	
  </head>

  <body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  	<!-- Brand -->
	  	<a class="navbar-brand" href="
                     """f'{opts.root_path}'
                     """
                     ">
	  		<img src="
                     """f'{opts.root_path}image/logo.png'
                     """
                     " alt="Logo" style="width:100px;">
	  	</a>  
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    	<span class="navbar-toggler-icon"></span>
	  	</button>
		<div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item active">
		        <a class="nav-link" href="
                     """f'{opts.root_path}'
                     """
                     ">Home <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="
                     """
                     f'{opts.root_path}download'
                     """
                     ">Download</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="
                     """
                     f'{opts.source_url}'
                     """
                     ">Sources</a>
		      </li>
		    </ul>
	  	</div>
	</nav>
    <div class="container"> 
        <div class="page-header"><h2>"""
                     f'{path_top_dir.name}'
                     """</h2>
        </div>
                 """)

    if(Path(opts.top_dir).absolute() != path_top_dir.absolute()):
        index_file.write(f"""
        <div class="row">
            <div class="col-sm-8">
                <div class="file"><i class="far fa-folder"></i> <a href="../index.html">..</a></div>
            </div>
            <div class="col-sm-2">
                <div class="type"> </div>
            </div>
            <div class="col-sm-2">
                <div class="type"></div>
            </div>
        </div>
    """)
    # sort dirs first
    sorted_entries = sorted(path_top_dir.glob(glob_patt), key=lambda p: (p.is_file(), p.name))

    entry: Path
    for entry in sorted_entries:

        # don't include index.html in the file listing
        if entry.name.lower() == opts.output_file.lower():
            continue

        if not opts.include_hidden and entry.name.startswith('.'):
            continue

        if entry.is_dir() and opts.recursive:
            process_dir(entry, opts)

        # From Python 3.6, os.access() accepts path-like objects
        if (not entry.is_symlink()) and not os.access(str(entry), os.W_OK):
            print(f"*** WARNING *** entry {entry.absolute()} is not writable! SKIPPING!")
            continue
        if opts.verbose:
            print(f'{entry.absolute()}')

        size_bytes = -1  ## is a folder
        size_pretty = '&mdash;'
        last_modified = '-'
        last_modified_human_readable = '-'
        last_modified_iso = ''
        try:
            if entry.is_file():
                size_bytes = entry.stat().st_size
                size_pretty = pretty_size(size_bytes)

            if entry.is_dir() or entry.is_file():
                last_modified = datetime.datetime.fromtimestamp(entry.stat().st_mtime).replace(microsecond=0)
                last_modified_iso = last_modified.isoformat()
                last_modified_human_readable = last_modified.strftime("%Y-%m-%d %H:%M:%S")

        except Exception as e:
            print('ERROR accessing file name:', e, entry)
            continue

        entry_path = str(entry.name)

        if entry.is_dir() and not entry.is_symlink():
            entry_type = 'folder'
            entry_type_fa = 'far fa-folder'
            if os.name not in ('nt',):
                # append trailing slash to dirs, unless it's windows
                entry_path = os.path.join(entry.name, '')+'index.html'

        elif entry.is_file() and (entry.name.endswith('.zip') or entry.name.endswith('.gz')):
            entry_type = 'file-shortcut'
            entry_type_fa = 'far fa-file-archive'

        else:
            entry_type = 'file'
            entry_type_fa = 'far fa-file'


        index_file.write(f"""
        <div class="row">
            <div class="col-sm-8">
                <div class="file"><i class="{entry_type_fa}"></i> <a href="{quote(entry_path)}">{entry.name}</a></div>
            </div>
            <div class="col-sm-2">
                <div class="type"> <time datetime="{last_modified_iso}">{last_modified_human_readable}</time> </div>
            </div>
            <div class="col-sm-2">
                <div class="type"><i class="far fa-hdd"></i> {size_pretty}</div>
            </div>
        </div>
""")

    index_file.write("""
    </div> <!-- /container -->
  </body>
</html>""")
    if index_file:
        index_file.close()


# bytes pretty-printing
UNITS_MAPPING = [
    (1024 ** 5, ' PB'),
    (1024 ** 4, ' TB'),
    (1024 ** 3, ' GB'),
    (1024 ** 2, ' MB'),
    (1024 ** 1, ' KB'),
    (1024 ** 0, (' byte', ' bytes')),
]


def pretty_size(bytes, units=UNITS_MAPPING):
    """Human-readable file sizes.
    ripped from https://pypi.python.org/pypi/hurry.filesize/
    """
    for factor, suffix in units:
        if bytes >= factor:
            break
    amount = int(bytes / factor)

    if isinstance(suffix, tuple):
        singular, multiple = suffix
        if amount == 1:
            suffix = singular
        else:
            suffix = multiple
    return str(amount) + suffix


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='''DESCRIPTION: This script generates an .html index  of files within a directory (recursive is OFF by default). Start from current dir or from folder passed as first positional argument. Optionally filter by file types with --filter "*.py"
Email josh dot brunty at marshall dot edu for additional help. ''')

    parser.add_argument('top_dir',
                        nargs='?',
                        action='store',
                        help='top folder from which to start generating indexes, '
                             'use current folder if not specified',
                        default=os.getcwd())

    parser.add_argument('--filter', '-f',
                        help='only include files matching glob',
                        required=False)

    parser.add_argument('--output-file', '-o',
                        metavar='filename',
                        default=DEFAULT_OUTPUT_FILE,
                        help=f'Custom output file, by default "{DEFAULT_OUTPUT_FILE}"')

    parser.add_argument('--recursive', '-r',
                        action='store_true',
                        help="recursively process nested dirs (FALSE by default)",
                        required=False)

    parser.add_argument('--include-hidden', '-i',
                        action='store_true',
                        help="include hidden files and folders (FALSE by default)",
                        required=False)
    
    parser.add_argument('--root-path',
                        metavar='rootpath',
                        default=DEFAULT_ROOT_PATH,
                        help=f'Path to root on the deployed web site, by default "{DEFAULT_ROOT_PATH}"')    
    parser.add_argument('--source-url',
                        metavar='sourceurl',
                        default=DEFAULT_SOURCE_URL,
                        help=f'URL of the git containing the source of this web site/application, by default "{DEFAULT_SOURCE_URL}"')
    
    parser.add_argument('--title',
                        metavar='title',
                        default=DEFAULT_TITLE,
                        help=f'Title of the generated pages, by default "{DEFAULT_TITLE}"')  
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='***WARNING: can take longer time with complex file tree structures on slow terminals***'
                             ' verbosely list every processed file',
                        required=False)

    config = parser.parse_args(sys.argv[1:])
    process_dir(config.top_dir, config)